# translation of ru.po to Russian
# Menu section translation
# Copyright (C) 2003
# This file is distributed under the same license as the menu package.
#
# Bill Allombert <ballombe@debian.org>, 2003.
# Russian L10N Team <debian-l10n-russian@lists.debian.org>, 2005.
# Yuri Kozlov <kozlov.y@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ru_new\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2007-07-26 22:00+0400\n"
"Last-Translator: Yuri Kozlov <kozlov.y@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
msgid "Action"
msgstr "Экшн"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
msgid "Action games"
msgstr "Динамичные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr "Администрирование"

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr "Изменение системных установок (влияет на всех пользователей)"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
#, fuzzy
msgid "preferences-system"
msgstr "Параметры"

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr "Детские"

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr "Игры для детей"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
msgid "Cards"
msgstr "Карты"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr "Карточные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr "Debian"

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr "Меню Debian"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "Симуляторы"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
msgid "Simulation games"
msgstr "Игры-симуляторы"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "Настольные"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
msgid "Board games"
msgstr "Настольные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr "Hаука"

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
msgid "Scientific applications"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "Стратегии"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
msgid "Strategy games"
msgstr "Стратегические игры"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "Аркады"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr "Аркадные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr "Параметры"

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr "Персональные предпочтения"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
#, fuzzy
msgid "preferences-desktop"
msgstr "Параметры"

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr "Тетрисы"

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr "Тетрисоподобные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "Спортивные"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
msgid "Sports games"
msgstr "Спортивные игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr "Логические"

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr "Логические игры и головоломки"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "Приключения"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
msgid "Adventure style games"
msgstr "Приключенческие игры"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr "Ролевые"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr "Ролевые игры"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
